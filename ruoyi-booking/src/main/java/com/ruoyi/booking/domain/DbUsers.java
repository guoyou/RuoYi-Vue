package com.ruoyi.booking.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * 会员用户对象 db_users
 * 
 * @author ruoyi
 * @date 2021-12-03
 */
public class DbUsers extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 用户ID */
    private Long userId;

    /** 用户微信openid */
    @Excel(name = "用户微信openid")
    private String openid;

    /** 用户名称 */
    @Excel(name = "用户名称")
    private String name;

    /** 手机号码 */
    @Excel(name = "手机号码")
    private String mobile;

    /** 头像 */
    @Excel(name = "头像")
    private String avatar;

    /** 性别 1男 2女 */
    @Excel(name = "性别 1男 2女")
    private Integer gender;

    /** 状态 1正常 0锁定 */
    @Excel(name = "状态 1正常 0锁定")
    private Integer status;

    /** 添加时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "添加时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date addTime;

    /** 删除状态 1删除0正常 */
    @Excel(name = "删除状态 1删除0正常")
    private Integer del;

    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setOpenid(String openid) 
    {
        this.openid = openid;
    }

    public String getOpenid() 
    {
        return openid;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setMobile(String mobile) 
    {
        this.mobile = mobile;
    }

    public String getMobile() 
    {
        return mobile;
    }
    public void setAvatar(String avatar) 
    {
        this.avatar = avatar;
    }

    public String getAvatar() 
    {
        return avatar;
    }
    public void setGender(Integer gender) 
    {
        this.gender = gender;
    }

    public Integer getGender() 
    {
        return gender;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setAddTime(Date addTime)
    {
        this.addTime = addTime;
    }

    public Date getAddTime()
    {
        return addTime;
    }
    public void setDel(Integer del) 
    {
        this.del = del;
    }

    public Integer getDel() 
    {
        return del;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("userId", getUserId())
            .append("openid", getOpenid())
            .append("name", getName())
            .append("mobile", getMobile())
            .append("avatar", getAvatar())
            .append("gender", getGender())
            .append("status", getStatus())
            .append("addTime", getAddTime())
            .append("del", getDel())
            .toString();
    }
}
