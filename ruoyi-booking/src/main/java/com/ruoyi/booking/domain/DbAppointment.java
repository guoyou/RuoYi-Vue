package com.ruoyi.booking.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 预约记录对象 db_appointment
 *
 * @author ruoyi
 * @date 2021-12-07
 */
public class DbAppointment extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 预约记录ID */
    private Long appointmentId;

    /** 预约手机号码 */
    @Excel(name = "预约手机号码")
    private String contactPhone;

    /** 预约号码 */
    @Excel(name = "预约号码")
    private String appointmentNum;

    /** 预约时间 */
    @Excel(name = "预约时间")
    private String appointmentTime;

    /** 菜谱编号 */
    @Excel(name = "菜谱编号")
    private Long menuId;

    private String name;

    private String photos;

    private Integer surplus;

    private Integer goodsCount;

    private String menuTime;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhotos() {
        return photos;
    }

    public void setPhotos(String photos) {
        this.photos = photos;
    }

    public Integer getSurplus() {
        return surplus;
    }

    public void setSurplus(Integer surplus) {
        this.surplus = surplus;
    }

    public Integer getGoodsCount() {
        return goodsCount;
    }

    public void setGoodsCount(Integer goodsCount) {
        this.goodsCount = goodsCount;
    }

    public String getMenuTime() {
        return menuTime;
    }

    public void setMenuTime(String menuTime) {
        this.menuTime = menuTime;
    }

    public void setAppointmentId(Long appointmentId)
    {
        this.appointmentId = appointmentId;
    }

    public Long getAppointmentId()
    {
        return appointmentId;
    }
    public void setContactPhone(String contactPhone)
    {
        this.contactPhone = contactPhone;
    }

    public String getContactPhone()
    {
        return contactPhone;
    }
    public void setAppointmentNum(String appointmentNum)
    {
        this.appointmentNum = appointmentNum;
    }

    public String getAppointmentNum()
    {
        return appointmentNum;
    }
    public void setAppointmentTime(String appointmentTime)
    {
        this.appointmentTime = appointmentTime;
    }

    public String getAppointmentTime()
    {
        return appointmentTime;
    }
    public void setMenuId(Long menuId)
    {
        this.menuId = menuId;
    }

    public Long getMenuId()
    {
        return menuId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("appointmentId", getAppointmentId())
                .append("contactPhone", getContactPhone())
                .append("appointmentNum", getAppointmentNum())
                .append("appointmentTime", getAppointmentTime())
                .append("menuId", getMenuId())
                .append("name", getName())
                .append("photos", getPhotos())
                .append("surplus", getSurplus())
                .append("goodsCount", getGoodsCount())
                .append("menuTime", getMenuTime())
                .toString();
    }
}