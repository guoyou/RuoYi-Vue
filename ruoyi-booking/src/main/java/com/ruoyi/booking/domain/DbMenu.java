package com.ruoyi.booking.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.math.BigDecimal;

/**
 * 菜单记录对象 db_menu
 *
 * @author ruoyi
 * @date 2021-12-07
 */
public class DbMenu extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 菜单ID */
    private Long menuId;

    /** 商品ID */
    @Excel(name = "商品ID")
    private Long goodsId;

    private String name;

    /** 菜单时间 */
    @Excel(name = "菜单时间")
    private String menuTime;

    /** 商品数量 */
    @Excel(name = "商品数量")
    private Long goodsCount;

    /** 剩余数量 */
    @Excel(name = "剩余数量")
    private Long surplus;

    private Long date;

    private String photos;

    private String specification;

    private BigDecimal price;

    private String code;

    public void setMenuId(Long menuId)
    {
        this.menuId = menuId;
    }

    public Long getMenuId()
    {
        return menuId;
    }
    public void setGoodsId(Long goodsId)
    {
        this.goodsId = goodsId;
    }

    public Long getGoodsId()
    {
        return goodsId;
    }
    public void setMenuTime(String menuTime)
    {
        this.menuTime = menuTime;
    }

    public String getMenuTime()
    {
        return menuTime;
    }
    public void setGoodsCount(Long goodsCount)
    {
        this.goodsCount = goodsCount;
    }

    public Long getGoodsCount()
    {
        return goodsCount;
    }
    public void setSurplus(Long surplus)
    {
        this.surplus = surplus;
    }

    public Long getSurplus()
    {
        return surplus;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhotos() {
        return photos;
    }

    public void setPhotos(String photos) {
        this.photos = photos;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("menuId", getMenuId())
                .append("goodsId", getGoodsId())
                .append("name", getName())
                .append("menuTime", getMenuTime())
                .append("goodsCount", getGoodsCount())
                .append("surplus", getSurplus())
                .append("date", getDate())
                .append("photos", getPhotos())
                .append("code", getCode())
                .append("specification", getSpecification())
                .append("price", getPrice())
                .toString();
    }
}