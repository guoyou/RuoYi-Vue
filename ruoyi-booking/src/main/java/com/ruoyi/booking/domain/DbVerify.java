package com.ruoyi.booking.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 核销记录对象 db_verify
 * 
 * @author ruoyi
 * @date 2021-12-03
 */
public class DbVerify extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 核销ID */
    private Long verifyId;

    /** 预约手机号 */
    @Excel(name = "预约手机号")
    private String contactPhone;

    /** 预约ID */
    @Excel(name = "预约ID")
    private Long appointmentId;

    /** 核销时间 */
    @Excel(name = "核销时间")
    private String verifyTime;

    public void setVerifyId(Long verifyId) 
    {
        this.verifyId = verifyId;
    }

    public Long getVerifyId() 
    {
        return verifyId;
    }
    public void setContactPhone(String contactPhone) 
    {
        this.contactPhone = contactPhone;
    }

    public String getContactPhone() 
    {
        return contactPhone;
    }
    public void setAppointmentId(Long appointmentId) 
    {
        this.appointmentId = appointmentId;
    }

    public Long getAppointmentId() 
    {
        return appointmentId;
    }
    public void setVerifyTime(String verifyTime) 
    {
        this.verifyTime = verifyTime;
    }

    public String getVerifyTime() 
    {
        return verifyTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("verifyId", getVerifyId())
            .append("contactPhone", getContactPhone())
            .append("appointmentId", getAppointmentId())
            .append("verifyTime", getVerifyTime())
            .toString();
    }
}
