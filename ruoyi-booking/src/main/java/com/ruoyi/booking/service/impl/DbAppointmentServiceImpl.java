package com.ruoyi.booking.service.impl;

import java.util.List;
import java.util.Map;

import com.ruoyi.booking.domain.DbMenu;
import com.ruoyi.booking.mapper.DbMenuMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.booking.mapper.DbAppointmentMapper;
import com.ruoyi.booking.domain.DbAppointment;
import com.ruoyi.booking.service.IDbAppointmentService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 预约记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-12-03
 */
@Service
public class DbAppointmentServiceImpl implements IDbAppointmentService 
{
    @Autowired
    private DbAppointmentMapper dbAppointmentMapper;

    @Autowired
    private DbMenuMapper dbMenuMapper;

    /**
     * 查询预约记录
     * 
     * @param appointmentId 预约记录主键
     * @return 预约记录
     */
    @Override
    public DbAppointment selectDbAppointmentByAppointmentId(Long appointmentId)
    {
        return dbAppointmentMapper.selectDbAppointmentByAppointmentId(appointmentId);
    }

    /**
     * 查询预约记录列表
     * 
     * @param map 预约记录
     * @return 预约记录
     */
    @Override
    public List<DbAppointment> selectDbAppointmentList(Map map)
    {
        return dbAppointmentMapper.selectDbAppointmentList(map);
    }

    /**
     * 新增预约记录
     * 
     * @param dbAppointment 预约记录
     * @return 结果
     */
    @Override
    public int insertDbAppointment(DbAppointment dbAppointment)
    {
        return dbAppointmentMapper.insertDbAppointment(dbAppointment);
    }

    @Transactional(rollbackFor = {Exception.class})
    @Override
    public int buildDbAppointment(DbAppointment dbAppointment) {
        List<DbAppointment> appointments = dbAppointmentMapper.selectDbAppointmentByCondition(dbAppointment);
        if (appointments != null && !appointments.isEmpty()) {
            return 0;
        }

        DbMenu dbMenu = dbMenuMapper.selectDbMenuByMenuId(dbAppointment.getMenuId());
        Long surplus = dbMenu.getSurplus();
        surplus = surplus -1;
        dbMenu.setSurplus(surplus);
        dbMenuMapper.updateDbMenu(dbMenu);

        return dbAppointmentMapper.insertDbAppointment(dbAppointment);
    }

    /**
     * 修改预约记录
     * 
     * @param dbAppointment 预约记录
     * @return 结果
     */
    @Override
    public int updateDbAppointment(DbAppointment dbAppointment)
    {
        return dbAppointmentMapper.updateDbAppointment(dbAppointment);
    }

    /**
     * 批量删除预约记录
     * 
     * @param appointmentIds 需要删除的预约记录主键
     * @return 结果
     */
    @Override
    public int deleteDbAppointmentByAppointmentIds(Long[] appointmentIds)
    {
        return dbAppointmentMapper.deleteDbAppointmentByAppointmentIds(appointmentIds);
    }

    /**
     * 删除预约记录信息
     * 
     * @param appointmentId 预约记录主键
     * @return 结果
     */
    @Override
    public int deleteDbAppointmentByAppointmentId(Long appointmentId)
    {
        return dbAppointmentMapper.deleteDbAppointmentByAppointmentId(appointmentId);
    }
}
