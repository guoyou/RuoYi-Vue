package com.ruoyi.booking.service;

import com.ruoyi.booking.domain.DbVerify;

import java.util.Date;
import java.util.List;

/**
 * 核销记录Service接口
 * 
 * @author ruoyi
 * @date 2021-12-03
 */
public interface IBookingConfigService
{
    /**
     * 设置预约起始时间
     * @param startTime
     */
    void setAppointmentStartTime(Date startTime);

    Date getAppointmentStartTime();

    /**
     * 设置预约结束时间
     * @param endTime
     */
    void setAppointmentEndTime(Date endTime);

    Date getAppointmentEndTime();

    /**
     * 设置领取起始时间
     * @param startTime
     */
    void setReceiveStartTime(Date startTime);

    Date getReceiveStartTime();

    /**
     * 设置领取结束时间
     * @param endTime
     */
    void setReceiveEndTime(Date endTime);

    Date getReceiveEndTime();

    /**
     * 设置开关
     * @param switchStatus
     */
    void setSwitch(boolean switchStatus);

    boolean getSwitch();
}
