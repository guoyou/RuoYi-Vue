package com.ruoyi.booking.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.booking.mapper.DbGoodsMapper;
import com.ruoyi.booking.domain.DbGoods;
import com.ruoyi.booking.service.IDbGoodsService;

/**
 * 商品Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-12-03
 */
@Service
public class DbGoodsServiceImpl implements IDbGoodsService 
{
    @Autowired
    private DbGoodsMapper dbGoodsMapper;

    /**
     * 查询商品
     * 
     * @param goodsId 商品主键
     * @return 商品
     */
    @Override
    public DbGoods selectDbGoodsByGoodsId(Long goodsId)
    {
        return dbGoodsMapper.selectDbGoodsByGoodsId(goodsId);
    }

    /**
     * 查询商品列表
     * 
     * @param dbGoods 商品
     * @return 商品
     */
    @Override
    public List<DbGoods> selectDbGoodsList(DbGoods dbGoods)
    {
        return dbGoodsMapper.selectDbGoodsList(dbGoods);
    }

    /**
     * 新增商品
     * 
     * @param dbGoods 商品
     * @return 结果
     */
    @Override
    public int insertDbGoods(DbGoods dbGoods)
    {
        return dbGoodsMapper.insertDbGoods(dbGoods);
    }

    /**
     * 修改商品
     * 
     * @param dbGoods 商品
     * @return 结果
     */
    @Override
    public int updateDbGoods(DbGoods dbGoods)
    {
        return dbGoodsMapper.updateDbGoods(dbGoods);
    }

    /**
     * 批量删除商品
     * 
     * @param goodsIds 需要删除的商品主键
     * @return 结果
     */
    @Override
    public int deleteDbGoodsByGoodsIds(Long[] goodsIds)
    {
        return dbGoodsMapper.deleteDbGoodsByGoodsIds(goodsIds);
    }

    /**
     * 删除商品信息
     * 
     * @param goodsId 商品主键
     * @return 结果
     */
    @Override
    public int deleteDbGoodsByGoodsId(Long goodsId)
    {
        return dbGoodsMapper.deleteDbGoodsByGoodsId(goodsId);
    }
}
