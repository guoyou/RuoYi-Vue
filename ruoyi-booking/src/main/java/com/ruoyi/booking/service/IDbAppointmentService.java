package com.ruoyi.booking.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.booking.domain.DbAppointment;

/**
 * 预约记录Service接口
 * 
 * @author ruoyi
 * @date 2021-12-03
 */
public interface IDbAppointmentService 
{
    /**
     * 查询预约记录
     * 
     * @param appointmentId 预约记录主键
     * @return 预约记录
     */
    public DbAppointment selectDbAppointmentByAppointmentId(Long appointmentId);

    /**
     * 查询预约记录列表
     * 
     * @param map 预约记录
     * @return 预约记录集合
     */
    public List<DbAppointment> selectDbAppointmentList(Map map);

    /**
     * 新增预约记录
     * 
     * @param dbAppointment 预约记录
     * @return 结果
     */
    public int insertDbAppointment(DbAppointment dbAppointment);

    public int buildDbAppointment(DbAppointment dbAppointment);

    /**
     * 修改预约记录
     * 
     * @param dbAppointment 预约记录
     * @return 结果
     */
    public int updateDbAppointment(DbAppointment dbAppointment);

    /**
     * 批量删除预约记录
     * 
     * @param appointmentIds 需要删除的预约记录主键集合
     * @return 结果
     */
    public int deleteDbAppointmentByAppointmentIds(Long[] appointmentIds);

    /**
     * 删除预约记录信息
     * 
     * @param appointmentId 预约记录主键
     * @return 结果
     */
    public int deleteDbAppointmentByAppointmentId(Long appointmentId);
}
