package com.ruoyi.booking.service.impl;

import com.ruoyi.booking.service.IBookingConfigService;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.SysConfig;
import com.ruoyi.system.mapper.SysConfigMapper;
import com.ruoyi.system.service.ISysConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class BookingConfigServiceImp implements IBookingConfigService {

    @Autowired
    ISysConfigService sysConfigService;
    @Autowired
    private SysConfigMapper configMapper;

    @Override
    public void setAppointmentStartTime(Date startTime) {
        SysConfig sysConfig = new SysConfig();
        sysConfig.setConfigKey("book.appointment.start");
        sysConfig.setConfigType("N");
        sysConfig.setConfigName("预约开始时间");
        sysConfig.setConfigValue(DateUtils.parseDateToStr(DateUtils.YYYY_MM_DD_HH_MM_SS, startTime));

        SysConfig configInDb = configMapper.selectConfig(sysConfig);
        if (configInDb != null) {
            sysConfig.setConfigId(configInDb.getConfigId());
            sysConfigService.updateConfig(sysConfig);
        }
        else {
            sysConfigService.insertConfig(sysConfig);
        }
    }

    @Override
    public Date getAppointmentStartTime() {
        String startTime = sysConfigService.selectConfigByKey("book.appointment.start");
        return DateUtils.parseDate(startTime);
    }

    @Override
    public void setAppointmentEndTime(Date endTime) {
        SysConfig sysConfig = new SysConfig();
        sysConfig.setConfigKey("book.appointment.end");
        sysConfig.setConfigType("N");
        sysConfig.setConfigName("预约结束时间");
        sysConfig.setConfigValue(DateUtils.parseDateToStr(DateUtils.YYYY_MM_DD_HH_MM_SS, endTime));
        SysConfig configInDb = configMapper.selectConfig(sysConfig);
        if (configInDb != null) {
            sysConfig.setConfigId(configInDb.getConfigId());
            sysConfigService.updateConfig(sysConfig);
        }
        else {
            sysConfigService.insertConfig(sysConfig);
        }
    }

    @Override
    public Date getAppointmentEndTime() {
        String endTime = sysConfigService.selectConfigByKey("book.appointment.end");
        return DateUtils.parseDate(endTime);
    }

    @Override
    public void setReceiveStartTime(Date startTime) {
        SysConfig sysConfig = new SysConfig();
        sysConfig.setConfigKey("book.receive.start");
        sysConfig.setConfigType("N");
        sysConfig.setConfigName("领取开始时间");
        sysConfig.setConfigValue(DateUtils.parseDateToStr(DateUtils.YYYY_MM_DD_HH_MM_SS, startTime));
        SysConfig configInDb = configMapper.selectConfig(sysConfig);
        if (configInDb != null) {
            sysConfig.setConfigId(configInDb.getConfigId());
            sysConfigService.updateConfig(sysConfig);
        }
        else {
            sysConfigService.insertConfig(sysConfig);
        }
    }

    @Override
    public Date getReceiveStartTime() {
        String startTime = sysConfigService.selectConfigByKey("book.receive.start");
        return DateUtils.parseDate(startTime);
    }

    @Override
    public void setReceiveEndTime(Date endTime) {
        SysConfig sysConfig = new SysConfig();
        sysConfig.setConfigKey("book.receive.end");
        sysConfig.setConfigType("N");
        sysConfig.setConfigName("领取结束时间");
        sysConfig.setConfigValue(DateUtils.parseDateToStr(DateUtils.YYYY_MM_DD_HH_MM_SS, endTime));
        SysConfig configInDb = configMapper.selectConfig(sysConfig);
        if (configInDb != null) {
            sysConfig.setConfigId(configInDb.getConfigId());
            sysConfigService.updateConfig(sysConfig);
        }
        else {
            sysConfigService.insertConfig(sysConfig);
        }
    }

    @Override
    public Date getReceiveEndTime() {
        String endTime = sysConfigService.selectConfigByKey("book.receive.end");
        return DateUtils.parseDate(endTime);
    }

    @Override
    public void setSwitch(boolean switchStatus) {
        SysConfig sysConfig = new SysConfig();
        sysConfig.setConfigKey("book.switch");
        sysConfig.setConfigType("N");
        sysConfig.setConfigName("开启状态");
        sysConfig.setConfigValue(Boolean.toString(switchStatus));
        SysConfig configInDb = configMapper.selectConfig(sysConfig);
        if (configInDb != null) {
            sysConfig.setConfigId(configInDb.getConfigId());
            sysConfigService.updateConfig(sysConfig);
        }
        else {
            sysConfigService.insertConfig(sysConfig);
        }
    }

    @Override
    public boolean getSwitch() {
        String switchString = sysConfigService.selectConfigByKey("book.switch");
        return Boolean.parseBoolean(switchString);
    }
}
