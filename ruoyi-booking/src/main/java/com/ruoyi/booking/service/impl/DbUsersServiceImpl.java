package com.ruoyi.booking.service.impl;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.booking.mapper.DbUsersMapper;
import com.ruoyi.booking.domain.DbUsers;
import com.ruoyi.booking.service.IDbUsersService;

/**
 * 会员用户Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-12-03
 */
@Service
public class DbUsersServiceImpl implements IDbUsersService 
{
    @Autowired
    private DbUsersMapper dbUsersMapper;

    /**
     * 查询会员用户
     * 
     * @param userId 会员用户主键
     * @return 会员用户
     */
    @Override
    public DbUsers selectDbUsersByUserId(Long userId)
    {
        return dbUsersMapper.selectDbUsersByUserId(userId);
    }

    /**
     * 查询会员用户列表
     * 
     * @param dbUsers 会员用户
     * @return 会员用户
     */
    @Override
    public List<DbUsers> selectDbUsersList(DbUsers dbUsers)
    {
        return dbUsersMapper.selectDbUsersList(dbUsers);
    }

    /**
     * 新增会员用户
     * 
     * @param dbUsers 会员用户
     * @return 结果
     */
    @Override
    public int insertDbUsers(DbUsers dbUsers)
    {
        return dbUsersMapper.insertDbUsers(dbUsers);
    }

    /**
     * 修改会员用户
     * 
     * @param dbUsers 会员用户
     * @return 结果
     */
    @Override
    public int updateDbUsers(DbUsers dbUsers)
    {
        return dbUsersMapper.updateDbUsers(dbUsers);
    }

    @Override
    public int saveOrUpdateUser(DbUsers dbUsers) {
        DbUsers query = new DbUsers();
        query.setOpenid(dbUsers.getOpenid());
        List<DbUsers> userList = dbUsersMapper.selectDbUsersList(query);
        if (userList == null || userList.isEmpty()) {
            dbUsers.setAddTime(new Date());
            dbUsers.setDel(0);
            dbUsers.setStatus(1);
            return dbUsersMapper.insertDbUsers(dbUsers);
        }


        return dbUsersMapper.updateDbUserByOpenid(dbUsers);
    }

    /**
     * 批量删除会员用户
     * 
     * @param userIds 需要删除的会员用户主键
     * @return 结果
     */
    @Override
    public int deleteDbUsersByUserIds(Long[] userIds)
    {
        return dbUsersMapper.deleteDbUsersByUserIds(userIds);
    }

    /**
     * 删除会员用户信息
     * 
     * @param userId 会员用户主键
     * @return 结果
     */
    @Override
    public int deleteDbUsersByUserId(Long userId)
    {
        return dbUsersMapper.deleteDbUsersByUserId(userId);
    }
}
