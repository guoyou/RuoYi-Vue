package com.ruoyi.booking.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.booking.mapper.DbMenuMapper;
import com.ruoyi.booking.domain.DbMenu;
import com.ruoyi.booking.service.IDbMenuService;

/**
 * 菜单记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-12-03
 */
@Service
public class DbMenuServiceImpl implements IDbMenuService 
{
    @Autowired
    private DbMenuMapper dbMenuMapper;

    /**
     * 查询菜单记录
     * 
     * @param menuId 菜单记录主键
     * @return 菜单记录
     */
    @Override
    public DbMenu selectDbMenuByMenuId(Long menuId)
    {
        return dbMenuMapper.selectDbMenuByMenuId(menuId);
    }

    /**
     * 查询菜单记录列表
     * 
     * @param map 菜单记录
     * @return 菜单记录
     */
    @Override
    public List<DbMenu> selectDbMenuList(Map map)
    {
        return dbMenuMapper.selectDbMenuList(map);
    }

    /**
     * 新增菜单记录
     * 
     * @param dbMenu 菜单记录
     * @return 结果
     */
    @Override
    public int insertDbMenu(DbMenu dbMenu)
    {
        return dbMenuMapper.insertDbMenu(dbMenu);
    }

    /**
     * 修改菜单记录
     * 
     * @param dbMenu 菜单记录
     * @return 结果
     */
    @Override
    public int updateDbMenu(DbMenu dbMenu)
    {
        return dbMenuMapper.updateDbMenu(dbMenu);
    }

    /**
     * 批量删除菜单记录
     * 
     * @param menuIds 需要删除的菜单记录主键
     * @return 结果
     */
    @Override
    public int deleteDbMenuByMenuIds(Long[] menuIds)
    {
        return dbMenuMapper.deleteDbMenuByMenuIds(menuIds);
    }

    /**
     * 删除菜单记录信息
     * 
     * @param menuId 菜单记录主键
     * @return 结果
     */
    @Override
    public int deleteDbMenuByMenuId(Long menuId)
    {
        return dbMenuMapper.deleteDbMenuByMenuId(menuId);
    }
}
