package com.ruoyi.booking.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.booking.mapper.DbVerifyMapper;
import com.ruoyi.booking.domain.DbVerify;
import com.ruoyi.booking.service.IDbVerifyService;

/**
 * 核销记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-12-03
 */
@Service
public class DbVerifyServiceImpl implements IDbVerifyService 
{
    @Autowired
    private DbVerifyMapper dbVerifyMapper;

    /**
     * 查询核销记录
     * 
     * @param verifyId 核销记录主键
     * @return 核销记录
     */
    @Override
    public DbVerify selectDbVerifyByVerifyId(Long verifyId)
    {
        return dbVerifyMapper.selectDbVerifyByVerifyId(verifyId);
    }

    /**
     * 查询核销记录列表
     * 
     * @param dbVerify 核销记录
     * @return 核销记录
     */
    @Override
    public List<DbVerify> selectDbVerifyList(DbVerify dbVerify)
    {
        return dbVerifyMapper.selectDbVerifyList(dbVerify);
    }

    /**
     * 新增核销记录
     * 
     * @param dbVerify 核销记录
     * @return 结果
     */
    @Override
    public int insertDbVerify(DbVerify dbVerify)
    {
        return dbVerifyMapper.insertDbVerify(dbVerify);
    }

    /**
     * 修改核销记录
     * 
     * @param dbVerify 核销记录
     * @return 结果
     */
    @Override
    public int updateDbVerify(DbVerify dbVerify)
    {
        return dbVerifyMapper.updateDbVerify(dbVerify);
    }

    /**
     * 批量删除核销记录
     * 
     * @param verifyIds 需要删除的核销记录主键
     * @return 结果
     */
    @Override
    public int deleteDbVerifyByVerifyIds(Long[] verifyIds)
    {
        return dbVerifyMapper.deleteDbVerifyByVerifyIds(verifyIds);
    }

    /**
     * 删除核销记录信息
     * 
     * @param verifyId 核销记录主键
     * @return 结果
     */
    @Override
    public int deleteDbVerifyByVerifyId(Long verifyId)
    {
        return dbVerifyMapper.deleteDbVerifyByVerifyId(verifyId);
    }
}
