package com.ruoyi.booking.service;

import java.util.List;
import com.ruoyi.booking.domain.DbGoods;

/**
 * 商品Service接口
 * 
 * @author ruoyi
 * @date 2021-12-03
 */
public interface IDbGoodsService 
{
    /**
     * 查询商品
     * 
     * @param goodsId 商品主键
     * @return 商品
     */
    public DbGoods selectDbGoodsByGoodsId(Long goodsId);

    /**
     * 查询商品列表
     * 
     * @param dbGoods 商品
     * @return 商品集合
     */
    public List<DbGoods> selectDbGoodsList(DbGoods dbGoods);

    /**
     * 新增商品
     * 
     * @param dbGoods 商品
     * @return 结果
     */
    public int insertDbGoods(DbGoods dbGoods);

    /**
     * 修改商品
     * 
     * @param dbGoods 商品
     * @return 结果
     */
    public int updateDbGoods(DbGoods dbGoods);

    /**
     * 批量删除商品
     * 
     * @param goodsIds 需要删除的商品主键集合
     * @return 结果
     */
    public int deleteDbGoodsByGoodsIds(Long[] goodsIds);

    /**
     * 删除商品信息
     * 
     * @param goodsId 商品主键
     * @return 结果
     */
    public int deleteDbGoodsByGoodsId(Long goodsId);
}
