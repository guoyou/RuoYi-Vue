package com.ruoyi.booking.mapper;

import java.util.List;
import com.ruoyi.booking.domain.DbUsers;

/**
 * 会员用户Mapper接口
 * 
 * @author ruoyi
 * @date 2021-12-03
 */
public interface DbUsersMapper 
{
    /**
     * 查询会员用户
     * 
     * @param userId 会员用户主键
     * @return 会员用户
     */
    public DbUsers selectDbUsersByUserId(Long userId);

    /**
     * 查询会员用户列表
     * 
     * @param dbUsers 会员用户
     * @return 会员用户集合
     */
    public List<DbUsers> selectDbUsersList(DbUsers dbUsers);

    /**
     * 新增会员用户
     * 
     * @param dbUsers 会员用户
     * @return 结果
     */
    public int insertDbUsers(DbUsers dbUsers);

    /**
     * 修改会员用户
     * 
     * @param dbUsers 会员用户
     * @return 结果
     */
    public int updateDbUsers(DbUsers dbUsers);

    /**
     * 通过openid修改用户
     * @param dbUsers
     * @return
     */
    public int updateDbUserByOpenid(DbUsers dbUsers);

    /**
     * 删除会员用户
     * 
     * @param userId 会员用户主键
     * @return 结果
     */
    public int deleteDbUsersByUserId(Long userId);

    /**
     * 批量删除会员用户
     * 
     * @param userIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDbUsersByUserIds(Long[] userIds);
}
