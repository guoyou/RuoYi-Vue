package com.ruoyi.booking.mapper;

import java.util.List;
import com.ruoyi.booking.domain.DbVerify;

/**
 * 核销记录Mapper接口
 * 
 * @author ruoyi
 * @date 2021-12-03
 */
public interface DbVerifyMapper 
{
    /**
     * 查询核销记录
     * 
     * @param verifyId 核销记录主键
     * @return 核销记录
     */
    public DbVerify selectDbVerifyByVerifyId(Long verifyId);

    /**
     * 查询核销记录列表
     * 
     * @param dbVerify 核销记录
     * @return 核销记录集合
     */
    public List<DbVerify> selectDbVerifyList(DbVerify dbVerify);

    /**
     * 新增核销记录
     * 
     * @param dbVerify 核销记录
     * @return 结果
     */
    public int insertDbVerify(DbVerify dbVerify);

    /**
     * 修改核销记录
     * 
     * @param dbVerify 核销记录
     * @return 结果
     */
    public int updateDbVerify(DbVerify dbVerify);

    /**
     * 删除核销记录
     * 
     * @param verifyId 核销记录主键
     * @return 结果
     */
    public int deleteDbVerifyByVerifyId(Long verifyId);

    /**
     * 批量删除核销记录
     * 
     * @param verifyIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDbVerifyByVerifyIds(Long[] verifyIds);
}
