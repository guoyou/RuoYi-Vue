package com.ruoyi.booking.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.booking.domain.DbMenu;

/**
 * 菜单记录Mapper接口
 * 
 * @author ruoyi
 * @date 2021-12-03
 */
public interface DbMenuMapper 
{
    /**
     * 查询菜单记录
     * 
     * @param menuId 菜单记录主键
     * @return 菜单记录
     */
    public DbMenu selectDbMenuByMenuId(Long menuId);

    /**
     * 查询菜单记录列表
     * 
     * @param map 菜单记录
     * @return 菜单记录集合
     */
    public List<DbMenu> selectDbMenuList(Map map);

    /**
     * 新增菜单记录
     * 
     * @param dbMenu 菜单记录
     * @return 结果
     */
    public int insertDbMenu(DbMenu dbMenu);

    /**
     * 修改菜单记录
     * 
     * @param dbMenu 菜单记录
     * @return 结果
     */
    public int updateDbMenu(DbMenu dbMenu);

    /**
     * 删除菜单记录
     * 
     * @param menuId 菜单记录主键
     * @return 结果
     */
    public int deleteDbMenuByMenuId(Long menuId);

    /**
     * 批量删除菜单记录
     * 
     * @param menuIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDbMenuByMenuIds(Long[] menuIds);
}
