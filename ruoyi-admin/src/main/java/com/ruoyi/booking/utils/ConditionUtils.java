package com.ruoyi.booking.utils;

import com.ruoyi.booking.domain.DbMenu;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ConditionUtils {
    public static Map buildDateCondition(Long date) {
        Calendar calendar = Calendar.getInstance();
        if (date != null) {
            calendar.setTimeInMillis(date);
        }

        calendar.add(Calendar.MONTH, -1);

        int minYear = calendar.get(Calendar.YEAR);
        int minMonth = calendar.get(Calendar.MONTH) + 1;
        int minDay = calendar.getActualMinimum(Calendar.DAY_OF_MONTH);

        calendar.add(Calendar.MONTH, 2);
        int maxYear = calendar.get(Calendar.YEAR);
        int maxMonth = calendar.get(Calendar.MONTH) + 1;
        int maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);


        String stringOfMinMonth = "";
        if (minMonth < 10) {
            stringOfMinMonth = "0" + minMonth;
        }
        else {
            stringOfMinMonth = "" + minMonth;
        }
        String stringOfMaxMonth = "";
        if (maxMonth < 10) {
            stringOfMaxMonth = "0" + maxMonth;
        }
        else {
            stringOfMaxMonth = "" + maxMonth;
        }
        Map map = new HashMap<>();
        if (minDay < 10) {
            map.put("minDay", minYear + "-" + stringOfMinMonth + "-0" + minDay);
        }
        else {
            map.put("minDay", minYear + "-" + stringOfMinMonth + "-" + minDay);
        }
        if (maxDay < 10) {
            map.put("maxDay", maxYear + "-" + stringOfMaxMonth + "-0" + maxDay);
        }
        else {
            map.put("maxDay", maxYear + "-" + stringOfMaxMonth + "-" + maxDay);
        }
        return map;
    }
}
