package com.ruoyi.booking.controller;

import com.ruoyi.booking.domain.DbVerify;
import com.ruoyi.booking.service.IDbVerifyService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 核销记录Controller
 * 
 * @author ruoyi
 * @date 2021-12-03
 */
@RestController
@RequestMapping("/booking/verify")
public class DbVerifyController extends BaseController
{
    @Autowired
    private IDbVerifyService dbVerifyService;

    /**
     * 查询核销记录列表
     */
    @PreAuthorize("@ss.hasPermi('booking:verify:list')")
    @GetMapping("/list")
    public TableDataInfo list(DbVerify dbVerify)
    {
        startPage();
        List<DbVerify> list = dbVerifyService.selectDbVerifyList(dbVerify);
        return getDataTable(list);
    }

    /**
     * 导出核销记录列表
     */
    @PreAuthorize("@ss.hasPermi('booking:verify:export')")
    @Log(title = "核销记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DbVerify dbVerify)
    {
        List<DbVerify> list = dbVerifyService.selectDbVerifyList(dbVerify);
        ExcelUtil<DbVerify> util = new ExcelUtil<DbVerify>(DbVerify.class);
        util.exportExcel(response, list, "核销记录数据");
    }

    /**
     * 获取核销记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('booking:verify:query')")
    @GetMapping(value = "/{verifyId}")
    public AjaxResult getInfo(@PathVariable("verifyId") Long verifyId)
    {
        return AjaxResult.success(dbVerifyService.selectDbVerifyByVerifyId(verifyId));
    }

    /**
     * 新增核销记录
     */
    @PreAuthorize("@ss.hasPermi('booking:verify:add')")
    @Log(title = "核销记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DbVerify dbVerify)
    {
        return toAjax(dbVerifyService.insertDbVerify(dbVerify));
    }

    /**
     * 修改核销记录
     */
    @PreAuthorize("@ss.hasPermi('booking:verify:edit')")
    @Log(title = "核销记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DbVerify dbVerify)
    {
        return toAjax(dbVerifyService.updateDbVerify(dbVerify));
    }

    /**
     * 删除核销记录
     */
    @PreAuthorize("@ss.hasPermi('booking:verify:remove')")
    @Log(title = "核销记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{verifyIds}")
    public AjaxResult remove(@PathVariable Long[] verifyIds)
    {
        return toAjax(dbVerifyService.deleteDbVerifyByVerifyIds(verifyIds));
    }
}
