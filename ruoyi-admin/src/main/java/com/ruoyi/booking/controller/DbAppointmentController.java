package com.ruoyi.booking.controller;

import com.ruoyi.booking.domain.DbAppointment;
import com.ruoyi.booking.service.IDbAppointmentService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 预约记录Controller
 * 
 * @author ruoyi
 * @date 2021-12-03
 */
@RestController
@RequestMapping("/booking/appointment")
public class DbAppointmentController extends BaseController
{
    @Autowired
    private IDbAppointmentService dbAppointmentService;

    /**
     * 查询预约记录列表
     */
    @PreAuthorize("@ss.hasPermi('booking:appointment:list')")
    @GetMapping("/list")
    public TableDataInfo list(Map map)
    {
        startPage();
        List<DbAppointment> list = dbAppointmentService.selectDbAppointmentList(map);
        return getDataTable(list);
    }

    /**
     * 导出预约记录列表
     */
    @PreAuthorize("@ss.hasPermi('booking:appointment:export')")
    @Log(title = "预约记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Map dbAppointment)
    {
        List<DbAppointment> list = dbAppointmentService.selectDbAppointmentList(dbAppointment);
        ExcelUtil<DbAppointment> util = new ExcelUtil<DbAppointment>(DbAppointment.class);
        util.exportExcel(response, list, "预约记录数据");
    }

    /**
     * 获取预约记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('booking:appointment:query')")
    @GetMapping(value = "/{appointmentId}")
    public AjaxResult getInfo(@PathVariable("appointmentId") Long appointmentId)
    {
        return AjaxResult.success(dbAppointmentService.selectDbAppointmentByAppointmentId(appointmentId));
    }

    /**
     * 新增预约记录
     */
    @PreAuthorize("@ss.hasPermi('booking:appointment:add')")
    @Log(title = "预约记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DbAppointment dbAppointment)
    {
        return toAjax(dbAppointmentService.insertDbAppointment(dbAppointment));
    }

    /**
     * 修改预约记录
     */
    @PreAuthorize("@ss.hasPermi('booking:appointment:edit')")
    @Log(title = "预约记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DbAppointment dbAppointment)
    {
        return toAjax(dbAppointmentService.updateDbAppointment(dbAppointment));
    }

    /**
     * 删除预约记录
     */
    @PreAuthorize("@ss.hasPermi('booking:appointment:remove')")
    @Log(title = "预约记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{appointmentIds}")
    public AjaxResult remove(@PathVariable Long[] appointmentIds)
    {
        return toAjax(dbAppointmentService.deleteDbAppointmentByAppointmentIds(appointmentIds));
    }
}
