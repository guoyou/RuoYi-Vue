package com.ruoyi.booking.controller;

import com.ruoyi.booking.domain.DbGoods;
import com.ruoyi.booking.service.IDbGoodsService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;

/**
 * 商品Controller
 * 
 * @author ruoyi
 * @date 2021-12-03
 */
@RestController
@RequestMapping("/booking/goods")
public class DbGoodsController extends BaseController
{
    @Autowired
    private IDbGoodsService dbGoodsService;

    /**
     * 查询商品列表
     */
    @PreAuthorize("@ss.hasPermi('booking:goods:list')")
    @GetMapping("/list")
    public TableDataInfo list(DbGoods dbGoods)
    {
        startPage();
        List<DbGoods> list = dbGoodsService.selectDbGoodsList(dbGoods);
        return getDataTable(list);
    }

    /**
     * 导出商品列表
     */
    @PreAuthorize("@ss.hasPermi('booking:goods:export')")
    @Log(title = "商品", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DbGoods dbGoods)
    {
        List<DbGoods> list = dbGoodsService.selectDbGoodsList(dbGoods);
        ExcelUtil<DbGoods> util = new ExcelUtil<DbGoods>(DbGoods.class);
        util.exportExcel(response, list, "商品数据");
    }

    /**
     * 获取商品详细信息
     */
    @PreAuthorize("@ss.hasPermi('booking:goods:query')")
    @GetMapping(value = "/{goodsId}")
    public AjaxResult getInfo(@PathVariable("goodsId") Long goodsId)
    {
        return AjaxResult.success(dbGoodsService.selectDbGoodsByGoodsId(goodsId));
    }

    /**
     * 新增商品
     */
    @PreAuthorize("@ss.hasPermi('booking:goods:add')")
    @Log(title = "商品", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DbGoods dbGoods)
    {
        if (dbGoods.getAddTime() == null) {
            dbGoods.setAddTime(new Date());
        }
        if (dbGoods.getDel() == null) {
            dbGoods.setDel(0);
        }
        if (dbGoods.getStatus() == null) {
            dbGoods.setStatus(0);
        }
        return toAjax(dbGoodsService.insertDbGoods(dbGoods));
    }

    /**
     * 修改商品
     */
    @PreAuthorize("@ss.hasPermi('booking:goods:edit')")
    @Log(title = "商品", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DbGoods dbGoods)
    {
        return toAjax(dbGoodsService.updateDbGoods(dbGoods));
    }

    /**
     * 删除商品
     */
    @PreAuthorize("@ss.hasPermi('booking:goods:remove')")
    @Log(title = "商品", businessType = BusinessType.DELETE)
	@DeleteMapping("/{goodsIds}")
    public AjaxResult remove(@PathVariable Long[] goodsIds)
    {
        return toAjax(dbGoodsService.deleteDbGoodsByGoodsIds(goodsIds));
    }
}
