package com.ruoyi.booking.controller;

import com.ruoyi.booking.domain.DbMenu;
import com.ruoyi.booking.service.IDbMenuService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 菜单记录Controller
 * 
 * @author ruoyi
 * @date 2021-12-03
 */
@RestController
@RequestMapping("/booking/menu")
public class DbMenuController extends BaseController
{
    @Autowired
    private IDbMenuService dbMenuService;

    /**
     * 查询菜单记录列表
     */
    @PreAuthorize("@ss.hasPermi('booking:menu:list')")
    @GetMapping("/list")
    public TableDataInfo list(DbMenu dbMenu)
    {
        Map map = buildDateCondition(dbMenu);
        List<DbMenu> list = dbMenuService.selectDbMenuList(map);
        return getDataTable(list);
    }

    private Map buildDateCondition(DbMenu dbMenu) {
        Calendar calendar = Calendar.getInstance();
        if (dbMenu.getDate() != null) {
            calendar.setTimeInMillis(dbMenu.getDate());
        }

        calendar.add(Calendar.MONTH, -1);

        int minYear = calendar.get(Calendar.YEAR);
        int minMonth = calendar.get(Calendar.MONTH) + 1;
        int minDay = calendar.getActualMinimum(Calendar.DAY_OF_MONTH);

        calendar.add(Calendar.MONTH, 2);
        int maxYear = calendar.get(Calendar.YEAR);
        int maxMonth = calendar.get(Calendar.MONTH) + 1;
        int maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);


        String stringOfMinMonth = "";
        if (minMonth < 10) {
            stringOfMinMonth = "0" + minMonth;
        }
        else {
            stringOfMinMonth = "" + minMonth;
        }
        String stringOfMaxMonth = "";
        if (maxMonth < 10) {
            stringOfMaxMonth = "0" + maxMonth;
        }
        else {
            stringOfMaxMonth = "" + maxMonth;
        }
        Map map = new HashMap<>();
        if (minDay < 10) {
            map.put("minDay", minYear + "-" + stringOfMinMonth + "-0" + minDay);
        }
        else {
            map.put("minDay", minYear + "-" + stringOfMinMonth + "-" + minDay);
        }
        if (maxDay < 10) {
            map.put("maxDay", maxYear + "-" + stringOfMaxMonth + "-0" + maxDay);
        }
        else {
            map.put("maxDay", maxYear + "-" + stringOfMaxMonth + "-" + maxDay);
        }
        return map;
    }

    /**
     * 导出菜单记录列表
     */
    @PreAuthorize("@ss.hasPermi('booking:menu:export')")
    @Log(title = "菜单记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DbMenu dbMenu)
    {
        Map map = buildDateCondition(dbMenu);
        List<DbMenu> list = dbMenuService.selectDbMenuList(map);
        ExcelUtil<DbMenu> util = new ExcelUtil<DbMenu>(DbMenu.class);
        util.exportExcel(response, list, "菜单记录数据");
    }

    /**
     * 获取菜单记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('booking:menu:query')")
    @GetMapping(value = "/{menuId}")
    public AjaxResult getInfo(@PathVariable("menuId") Long menuId)
    {
        return AjaxResult.success(dbMenuService.selectDbMenuByMenuId(menuId));
    }

    /**
     * 新增菜单记录
     */
    @PreAuthorize("@ss.hasPermi('booking:menu:add')")
    @Log(title = "菜单记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DbMenu dbMenu)
    {
        return toAjax(dbMenuService.insertDbMenu(dbMenu));
    }

    /**
     * 修改菜单记录
     */
    @PreAuthorize("@ss.hasPermi('booking:menu:edit')")
    @Log(title = "菜单记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DbMenu dbMenu)
    {
        return toAjax(dbMenuService.updateDbMenu(dbMenu));
    }

    /**
     * 删除菜单记录
     */
    @PreAuthorize("@ss.hasPermi('booking:menu:remove')")
    @Log(title = "菜单记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{menuIds}")
    public AjaxResult remove(@PathVariable Long[] menuIds)
    {
        return toAjax(dbMenuService.deleteDbMenuByMenuIds(menuIds));
    }
}
