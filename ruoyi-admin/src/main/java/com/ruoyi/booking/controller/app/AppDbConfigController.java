package com.ruoyi.booking.controller.app;

import com.ruoyi.booking.service.IBookingConfigService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 核销记录Controller
 * 
 * @author ruoyi
 * @date 2021-12-03
 */
@RestController
@RequestMapping("/app/booking/config")
public class AppDbConfigController extends BaseController
{
    @Autowired
    private IBookingConfigService bookingConfigService;



    /**
     * 获取核销记录详细信息
     */
    @GetMapping(value = "/info")
    public AjaxResult getInfo()
    {
        Date appointmentStartTime = bookingConfigService.getAppointmentStartTime();
        Date appointmentEndTime = bookingConfigService.getAppointmentEndTime();
        Date receiveStartTime = bookingConfigService.getReceiveStartTime();
        Date receiveEndTime = bookingConfigService.getReceiveEndTime();
        Boolean switchStatus = bookingConfigService.getSwitch();

        Map map = new HashMap();
        map.put("appointmentStartTime", appointmentStartTime);
        map.put("appointmentEndTime", appointmentEndTime);
        map.put("receiveStartTime", receiveStartTime);
        map.put("receiveEndTime", receiveEndTime);
        map.put("switchStatus", switchStatus);

        return AjaxResult.success(map);
    }
}
