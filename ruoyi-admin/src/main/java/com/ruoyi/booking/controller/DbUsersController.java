package com.ruoyi.booking.controller;

import com.ruoyi.booking.domain.DbUsers;
import com.ruoyi.booking.service.IDbUsersService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 会员用户Controller
 * 
 * @author ruoyi
 * @date 2021-12-03
 */
@RestController
@RequestMapping("/booking/users")
public class DbUsersController extends BaseController
{
    @Autowired
    private IDbUsersService dbUsersService;

    /**
     * 查询会员用户列表
     */
    @PreAuthorize("@ss.hasPermi('booking:users:list')")
    @GetMapping("/list")
    public TableDataInfo list(DbUsers dbUsers)
    {
        startPage();
        List<DbUsers> list = dbUsersService.selectDbUsersList(dbUsers);
        return getDataTable(list);
    }

    /**
     * 导出会员用户列表
     */
    @PreAuthorize("@ss.hasPermi('booking:users:export')")
    @Log(title = "会员用户", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DbUsers dbUsers)
    {
        List<DbUsers> list = dbUsersService.selectDbUsersList(dbUsers);
        ExcelUtil<DbUsers> util = new ExcelUtil<DbUsers>(DbUsers.class);
        util.exportExcel(response, list, "会员用户数据");
    }

    /**
     * 获取会员用户详细信息
     */
    @PreAuthorize("@ss.hasPermi('booking:users:query')")
    @GetMapping(value = "/{userId}")
    public AjaxResult getInfo(@PathVariable("userId") Long userId)
    {
        return AjaxResult.success(dbUsersService.selectDbUsersByUserId(userId));
    }

    /**
     * 新增会员用户
     */
    @PreAuthorize("@ss.hasPermi('booking:users:add')")
    @Log(title = "会员用户", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DbUsers dbUsers)
    {
        return toAjax(dbUsersService.insertDbUsers(dbUsers));
    }

    /**
     * 修改会员用户
     */
    @PreAuthorize("@ss.hasPermi('booking:users:edit')")
    @Log(title = "会员用户", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DbUsers dbUsers)
    {
        return toAjax(dbUsersService.updateDbUsers(dbUsers));
    }

    /**
     * 删除会员用户
     */
    @PreAuthorize("@ss.hasPermi('booking:users:remove')")
    @Log(title = "会员用户", businessType = BusinessType.DELETE)
	@DeleteMapping("/{userIds}")
    public AjaxResult remove(@PathVariable Long[] userIds)
    {
        return toAjax(dbUsersService.deleteDbUsersByUserIds(userIds));
    }
}
