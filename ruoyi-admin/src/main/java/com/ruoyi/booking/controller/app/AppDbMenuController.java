package com.ruoyi.booking.controller.app;

import com.ruoyi.booking.domain.DbMenu;
import com.ruoyi.booking.service.IDbMenuService;
import com.ruoyi.booking.utils.ConditionUtils;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 菜单记录Controller
 * 
 * @author ruoyi
 * @date 2021-12-03
 */
@RestController
@RequestMapping("/app/booking/menu")
public class AppDbMenuController extends BaseController
{
    @Autowired
    private IDbMenuService dbMenuService;

    /**
     * 查询菜单记录列表
     */
    @GetMapping("/list")
    public TableDataInfo list(DbMenu dbMenu)
    {
        Map map = ConditionUtils.buildDateCondition(dbMenu.getDate());
        List<DbMenu> list = dbMenuService.selectDbMenuList(map);
        return getDataTable(list);
    }

    /**
     * 获取菜单记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('booking:menu:query')")
    @GetMapping(value = "/{menuId}")
    public AjaxResult getInfo(@PathVariable("menuId") Long menuId)
    {
        return AjaxResult.success(dbMenuService.selectDbMenuByMenuId(menuId));
    }
}
