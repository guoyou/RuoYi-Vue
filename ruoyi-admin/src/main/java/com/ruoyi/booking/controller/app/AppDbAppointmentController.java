package com.ruoyi.booking.controller.app;

import com.ruoyi.booking.domain.DbAppointment;
import com.ruoyi.booking.service.IDbAppointmentService;
import com.ruoyi.booking.utils.ConditionUtils;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 预约记录Controller
 * 
 * @author ruoyi
 * @date 2021-12-03
 */
@RestController
@RequestMapping("/app/booking/appointment")
public class AppDbAppointmentController extends BaseController
{
    @Autowired
    private IDbAppointmentService dbAppointmentService;

    /**
     * 查询预约记录列表
     */
    @GetMapping("/list")
    public TableDataInfo list(DbAppointment dbAppointment)
    {
        Map map = ConditionUtils.buildDateCondition(new Date().getTime());
        List<DbAppointment> list = dbAppointmentService.selectDbAppointmentList(map);
        return getDataTable(list);
    }

    /**
     * 新增预约记录
     */
    @PostMapping
    public AjaxResult add(@RequestBody Map map)
    {
        DbAppointment dbAppointment = new DbAppointment();

        dbAppointment.setContactPhone((String) map.get("contactPhone"));
        dbAppointment.setAppointmentTime((String) map.get("appointmentTime"));
        Object menuIdObj = map.get("menuId");
        if (menuIdObj instanceof Integer) {
            Integer menuIdInt = (Integer) menuIdObj;
            dbAppointment.setMenuId(Long.valueOf(menuIdInt.longValue()));
        }
        else if (menuIdObj instanceof Long) {
            dbAppointment.setMenuId((Long) menuIdObj);
        }

        // 商品总数
        Integer goodsCount = (Integer) map.get("goodsCount");
        // 商品剩余数量
        Integer surplus = (Integer) map.get("surplus");
        // 商品编码
        String code = (String)map.get("code");

        int lengthOfTotal = Integer.toString(goodsCount).length();

        int nextNumber = goodsCount - surplus + 1;
        int lengthOfNextNumber = Integer.toString(nextNumber).length();

        if (lengthOfTotal > lengthOfNextNumber) {
            // 生成预约号码
            String appointmentNum = code + String.join("", Collections.nCopies(lengthOfTotal - lengthOfNextNumber, "0")).concat(String.valueOf(nextNumber));
            dbAppointment.setAppointmentNum(appointmentNum);
        }
        else if (lengthOfTotal == lengthOfNextNumber){
            dbAppointment.setAppointmentNum(code + String.valueOf(nextNumber));
        }
        else {
            dbAppointment.setAppointmentNum(code + String.valueOf(Math.random() * Math.pow(100, lengthOfTotal)));
        }

        return toAjax(dbAppointmentService.buildDbAppointment(dbAppointment));
    }
}
