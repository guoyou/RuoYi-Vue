-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('核销记录', '2000', '4', 'verify', 'booking/verify/index', 1, 0, 'C', '0', '0', 'booking:verify:list', '#', 'admin', sysdate(), '', null, '核销记录菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('核销记录查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'booking:verify:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('核销记录新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'booking:verify:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('核销记录修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'booking:verify:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('核销记录删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'booking:verify:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('核销记录导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'booking:verify:export',       '#', 'admin', sysdate(), '', null, '');