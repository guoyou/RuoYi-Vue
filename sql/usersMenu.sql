-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('会员用户', '2000', '5', 'users', 'booking/users/index', 1, 0, 'C', '0', '0', 'booking:users:list', '#', 'admin', sysdate(), '', null, '会员用户菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('会员用户查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'booking:users:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('会员用户新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'booking:users:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('会员用户修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'booking:users:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('会员用户删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'booking:users:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('会员用户导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'booking:users:export',       '#', 'admin', sysdate(), '', null, '');